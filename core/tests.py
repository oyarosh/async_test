#!/usr/bin/python3
import unittest
import asyncio
import time

from core.utils import RedisConnection


class CoreTestCase(unittest.TestCase):

    def setUp(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(None)

    def tearDown(self):
        self.loop.close()

    def test_protocol_set_method(self):
        async def set_test_func():
            conn = await RedisConnection.conn_initialize(loop=self.loop)
            result = await conn.excecute_cmd('set', [b'foo', b'bar'])
            self.assertEqual(result, b'OK')
            await conn.excecute_cmd('set', [b'foo1', b'bar1', b'expire=1'])
            result = await conn.excecute_cmd('get', [b'foo1'])
            self.assertEqual(result, b'bar1')
            time.sleep(1)
            result = await conn.excecute_cmd('get', [b'foo1'])
            self.assertEqual(result, None)
            await conn.excecute_cmd('set', [b'foo', b'bar', b'expire=1'])
            conn.close()
        self.loop.run_until_complete(set_test_func())
