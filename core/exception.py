#!/usr/bin/python3


class ConnectionLostError(Exception):
    def __init__(self, message='Lost connection'):
        super().__init__(message)


class MissedCMD(Exception):
    pass
