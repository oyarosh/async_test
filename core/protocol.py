#!/usr/bin/python3
import asyncio
import logging

from collections import deque
from functools import wraps

from asyncio.queues import Queue
from asyncio.futures import Future
from asyncio.streams import StreamReader

from .exception import ConnectionLostError


class PubSubReply(object):
    """
        Received pubsub message.
    """
    def __init__(self, channel, value):
        self._channel = channel
        self._value = value

    @property
    def channel(self):
        return self._channel

    @property
    def value(self):
        return self._value

    def __eq__(self, other):
        return (self._channel == other._channel and
                self._value == other._value)


class MultiBulkReply(object):
    """
      Multi bulk reply container
    """
    def __init__(self, protocol, count, loop=None):
        self._loop = loop or asyncio.get_event_loop()

        #: Buffer of incoming, undelivered data, received from the parser.
        self._data_queue = []

        #: Contains (read_count, Future, decode_flag, one_only_flag) tuples.
        self._f_queue = deque()

        self.protocol = protocol
        self.count = int(count)

    def _feed_received(self, item):
        """
            Feed entry for the parser.
        """
        # Push received items on the queue
        self._data_queue.append(item)
        self._flush()

    def _flush(self):
        """
            Answer read queries when we have enough data in our multibulk reply.
        """
        # As long as we have more data in our queue then we require for a read
        # query -> answer queries.
        while self._f_queue and self._f_queue[0][0] <= len(self._data_queue):
            # Pop query.
            count, f, decode, one_only = self._f_queue.popleft()

            # Slice data buffer.
            data, self._data_queue = self._data_queue[:count], self._data_queue[count:]

            # When the decode flag is given, decode bytes to native types.
            if decode:
                data = [self._decode(d) for d in data]

            # When one_only flag has been given, don't return an array.
            if one_only:
                assert len(data) == 1
                f.set_result(data[0])
            else:
                f.set_result(data)

    def _decode(self, result):
        """
            Decode bytes to native Python types.
        """
        if isinstance(result, (int, float, MultiBulkReply)):
            return result
        elif isinstance(result, bytes):
            return result.decode('utf-8')
        elif result is None:
            return result
        else:
            raise AssertionError('Invalid type: %r' % type(result))

    def _read(self, decode=True, count=1, _one=False):
        """
            Do read operation on the queue. Return future.
        """
        f = Future(loop=self.protocol._loop)
        self._f_queue.append((count, f, decode, _one))

        # If there is enough data on the queue, answer future immediately.
        self._flush()

        return f

    def __iter__(self):
        """
            Iterate over the reply. This yields coroutines of the decoded packets.
            It decodes bytes automatically using protocol.decode_to_native.
        """
        for i in range(self.count):
            yield self._read(_one=True)


class ProtocolRedis(asyncio.Protocol):
    """
        Protocol for Redis (TCP)
    """
    def __init__(self, lost_conn_callback=None, loop=None):
        self._lost_conn_callback = lost_conn_callback
        self._loop = loop or asyncio.get_event_loop()

        self.transport = None
        self._queue = deque()
        self._messages_queue = None  # Pubsub queue
        self._is_connected = False

        # Pubsub state
        self._in_pubsub = False
        self._subscription = None
        self._pubsub_channels = set()

        self._line_received_handlers = {
            b'+': self._handle_status_reply,
            b'$': self._handle_bulk_reply,
            b'*': self._handle_multi_bulk_reply,
            b':': self._handle_int_reply,
        }

    def connection_made(self, transport):
        self.transport = transport
        self._is_connected = True
        logging.info('Redis connection made')

        # Start parsing reader stream.
        self._reader = StreamReader(loop=self._loop)
        self._reader.set_transport(transport)
        self._reader_f = asyncio.ensure_future(self._reader_coroutine(), loop=self._loop)

        async def initialize():
            #  If we are in pubsub mode, send channel subscriptions again.
            if self._in_pubsub:
                if self._pubsub_channels:
                    await self._subscribe(self._subscription, list(self._pubsub_channels))

        asyncio.ensure_future(initialize(), loop=self._loop)

    def data_received(self, data):
        """
            Process data received from Redis server.
        """
        self._reader.feed_data(data)

    def connection_lost(self, exc):
        if exc is None:
            self._reader.feed_eof()
        else:
            logging.info("Connection lost with exeption: %s" % exc)
            self._reader.set_exception(exc)

        logging.info('Redis connection lost')

        # Call lost_conn_callback callback
        if self._lost_conn_callback:
            self._lost_conn_callback()

    async def _reader_coroutine(self):
        """
            Redis response process
        """
        while True:
            try:
                await self._handle_item(self._push_answer)
            except ConnectionLostError:
                return
            except asyncio.streams.IncompleteReadError:
                return

    async def _handle_status_reply(self, cb):
        line = (await self._reader.readline()).rstrip(b'\r\n')
        cb(line)

    async def _handle_bulk_reply(self, cb):
        length = int((await self._reader.readline()).rstrip(b'\r\n'))
        if length == -1:
            # None bulk reply
            cb(None)
        else:
            # Read data
            data = await self._reader.readexactly(length)
            cb(data)

            # Ignore trailing newline.
            remaining = await self._reader.readline()
            assert remaining.rstrip(b'\r\n') == b''

    async def _handle_int_reply(self, cb):
        line = (await self._reader.readline()).rstrip(b'\r\n')
        cb(int(line))

    async def _handle_multi_bulk_reply(self, cb):
        count = int((await self._reader.readline()).rstrip(b'\r\n'))

        reply = MultiBulkReply(self, count, loop=self._loop)

        if self._in_pubsub:
            asyncio.ensure_future(self._handle_pubsub_multibulk_reply(reply), loop=self._loop)
        else:
            cb(reply)

        # Wait for all multi bulk reply content.
        for i in range(count):
            await self._handle_item(reply._feed_received)

    async def _handle_pubsub_multibulk_reply(self, multibulk_reply):
        # Read first item of the multi bulk reply raw.
        type = await multibulk_reply._read(decode=False, _one=True)

        if type == b'message':
            channel, value = await multibulk_reply._read(count=2)
            await self._subscription._messages_queue.put(PubSubReply(channel, value))

    async def _handle_item(self, cb):
        c = await self._reader.readexactly(1)
        if c:
            await self._line_received_handlers[c](cb)
        else:
            raise ConnectionLostError(None)

    def _push_answer(self, answer):
        """
            Answer future at the queue.
        """
        f = self._queue.popleft()

        if isinstance(answer, Exception):
            f.set_exception(answer)
        else:
            f.set_result(answer)

    # Redis general methods
    def _send_command(self, args):
        """
            Send Redis request command.
        """
        data = []

        # Serialize and write header (number of arguments.)
        data += [b'*', str(len(args)).encode('ascii'), b'\r\n']

        # Write arguments.
        for arg in args:
            data += [ b'$', str(len(arg)).encode('ascii'), b'\r\n', arg, b'\r\n' ]

        self.transport.write(b''.join(data))

    async def _get_answer(self, answer_f, _bypass=False):
        """
            Return an answer to the pipelined query.
        """
        return await answer_f

    async def _query(self, *args, _bypass=False, set_blocking=False):
        """
            Wrapper for _send_command and _get_answer.
            It return the result.
        """

        if not self._is_connected:
            raise ConnectionLostError

        # Add a new future to our answer queue.
        answer_f = Future(loop=self._loop)
        self._queue.append(answer_f)

        # Send command
        self._send_command(args)

        # Receive answer.
        result = await self._get_answer(answer_f, _bypass=_bypass)

        return result

    # Redis commands

    def set(self, key, value, expire=None):
        """
            Set the string value of a key
        """
        params = [b'set', key, value]
        if expire is not None:
            expire = expire.decode('utf-8').replace('expire=', '')
            params.extend((b'ex', expire.encode('ascii')))
        return self._query(*params)

    def get(self, key):
        """
            # Get the value of a key
        """
        return self._query(b'get', key)

    def delete(self, *keys):
        """
            Delete a key/keys
        """
        return self._query(b'del', *keys)

    def keys(self, pattern):
        """
            Find all values matching keys pattern.
        """
        return self._query(b'keys', pattern)

    def subscribe(self):
        """
            Start subscribe to channel.
        """
        subscription = Subscription(self)

        self._in_pubsub = True
        self._subscription = subscription

        return subscription

    def _subscribe(self, channels):
        self._pubsub_channels |= set(channels)
        return self._pubsub_method('subscribe', channels)

    def publish(self, channel, message):
        """
            Post a message to a channel
        """
        return self._query(b'publish', channel, message)

    async def _pubsub_method(self, method, params):
        self._send_command([method.encode('ascii')] + list(map(lambda x: x.encode('utf-8'), params)))


class Subscription:
    """
        Pubsub subscription
    """
    def __init__(self, protocol):
        self.protocol = protocol
        self._messages_queue = Queue(loop=protocol._loop)

    @wraps(ProtocolRedis._subscribe)
    def subscribe(self, channels):
        return self.protocol._subscribe(channels)

    async def next_published(self):
        return (await self._messages_queue.get())
