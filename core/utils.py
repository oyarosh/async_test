#!/usr/bin/python3
import asyncio
import logging
import concurrent.futures

from .protocol import ProtocolRedis, MultiBulkReply, PubSubReply
from .exception import MissedCMD


class RedisConnection(object):
    """
        Establish connection to Redis with protocol
        (ex: core.protocol.ProtocolRedis)
    """
    @classmethod
    async def conn_initialize(cls, host='127.0.0.1', port='6379', db=0,
                              cls_protocol=ProtocolRedis, loop=None):
        """
            :param host: Redis host, could be domain
            :type host: str
            :param port: TCP port of Redis
            :type port: int
            :param db: Redis db for connection
            :type db: int
            :param cls_protocol: Redis protocol
            :type cls_protocol: :class:`~core.protocol.ProtocolRedis`
            :param loop: asyncio event loop
            :type loop: :class:`asyncio.unix_events._UnixSelectorEventLoop`
        """
        conn = cls()

        conn.host = host
        conn.port = port
        conn._loop = loop or asyncio.get_event_loop()
        conn._closed = False

        def lost_conn():
            if not conn._closed:
                asyncio.ensure_future(conn._connect(), loop=conn._loop)

        conn.protocol = cls_protocol(lost_conn_callback=lost_conn, loop=conn._loop)

        # connection
        await conn._connect()

        return conn

    async def _connect(self):
        while True:
            try:
                logging.info('Connect to Redis')
                await self._loop.create_connection(lambda: self.protocol, self.host, self.port)
                return
            except OSError:
                logging.info('Redis connection raise OSError. Retrying in %i seconds' % 60)
                await asyncio.sleep(interval, loop=self._loop)

    @property
    def transport(self):
        return self.protocol.transport

    def excecute_cmd(self, cmd, params):
        cmd = getattr(self.protocol, cmd)
        return cmd(*params)

    def close(self):
        self._closed = True

        if self.protocol.transport:
            self.protocol.transport.close()


class ServerTCP(object):
    """
        ServerTCP server class
    """
    def __init__(self, host, port, loop=None):
        self._loop = loop or asyncio.get_event_loop() 
        self._server = asyncio.start_server(self.handle_connection, host=host, port=port)

    def start(self, and_loop=True):
        self._server = self._loop.run_until_complete(self._server)
        logging.info('Listening established on {0}'.format(self._server.sockets[0].getsockname()))
        if and_loop:
            self._loop.run_forever()

    def stop(self, and_loop=True):
        self._server.close()
        if and_loop:
            self._loop.close()

    def prepare_cmd(self, data):
        try:
            prep_data = data.rstrip(b')\r\n').decode('utf-8').split('(')
            cmd = prep_data[0]
            params = [x.encode('utf-8') for x in prep_data[1].split(', ')]
        except IndexError:
            raise MissedCMD
        except UnicodeDecodeError:
            raise MissedCMD
        return (cmd, params)

    def process_result(self, result):
        if isinstance(result, int):
            result = str(result).encode('utf-8')
        elif result is None:
            result = b'No data'
        elif isinstance(result, MultiBulkReply):
            processed_result = b''
            for item in result.__iter__():
                processed_result += item.result().encode('utf-8') + b'\r\n'
            result = processed_result
        elif isinstance(result, PubSubReply):
            processed_result = 'channel: {0}, value: {1}\r\n'\
                .format(result.channel, result.value)\
                .encode('utf-8')
            result = processed_result
        return result

    async def handle_connection(self, reader, writer):
        peername = writer.get_extra_info('peername')
        logging.info('Accepted connection from {}'.format(peername))
        conn = await RedisConnection.conn_initialize()
        writer.write(b'''
            Supported commands:
                - set(key, value, expire=None),
                - get(key),
                - delete(key1, key2),
                - keys(*),
                - start_subscribe(channel)
                - publish(channel, msg)\r\n''')
        while not reader.at_eof():
            try:
                data = await asyncio.wait_for(reader.readline(), timeout=20)
                cmd, params = self.prepare_cmd(data)
                if cmd == 'start_subscribe':
                    subscription = conn.protocol.subscribe()
                    await subscription.subscribe(
                        [x.decode('utf-8') for x in params])
                    while True:
                        result = await subscription.next_published()
                        writer.write(self.process_result(result))
                else:
                    result = await conn.excecute_cmd(cmd, params)
                    writer.write(self.process_result(result))
                writer.write(b'\r\n')
            except MissedCMD:
                writer.write(b'Check your command and try again\r\n')
            except concurrent.futures.TimeoutError:
                break
        conn.close()
        writer.close()
