# README #

Task describtion:  

Необходимо написать сервис хранения данных в памяти по схеме ключ-значение.  
Рекомендуется использовать в реализации только стандартные библиотеки python 3 (в частности asyncio)
Протокол должен совпадать с протоколом Redis (TCP соединение, асинхронный обмен текстовыми сообщениями).  

Функции которые нужно поддерживать из протокола  

GET <key>  
SET <key> <value>  
DELETE <key>  
KEYS <pattern>  

SUBSCRIBE <channel>  
PUBLISH <channel> <message>  

### How do I get set up? ###

0) in terminal: python3 start.py  
1) in other terminal: telnet 127.0.0.1 2017  
