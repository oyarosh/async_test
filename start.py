#!/usr/bin/python3
from __future__ import absolute_import

import logging
import unittest

from core.utils import ServerTCP
from core.tests import CoreTestCase


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    test_run = input("Run only one test Y/n (default=n)")
    if test_run and test_run == 'Y':
        suite = unittest.TestLoader().loadTestsFromTestCase(CoreTestCase)
        unittest.TextTestRunner(verbosity=2).run(suite)
    else:
        server = ServerTCP('127.0.0.1', 2017)
        try:
            server.start()
        except KeyboardInterrupt:
            pass  # Press Ctrl+C to stop
        finally:
            server.stop()
